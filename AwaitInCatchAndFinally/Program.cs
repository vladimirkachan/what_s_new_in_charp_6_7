﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AwaitInCatchAndFinally
{
    class Program
    {
        public static async void TestFunctionAsync()
        {
            try
            {
                throw new Exception();
            }
            catch
            {
                Console.WriteLine("Start Task Catch!");
                await Task.Delay(1000);
                Console.WriteLine("Done Task Catch!");
            }
            finally
            {
                Console.WriteLine("Start Task Finally!");
                await Task.Delay(1000);
                Console.WriteLine("Done Task Finally!");
            }
        }
        static void Main(string[] args)
        {
            TestFunctionAsync();
            Console.WriteLine("Done!");
            Thread.Sleep(2000);
            Console.ReadKey();
        }
    }
}
