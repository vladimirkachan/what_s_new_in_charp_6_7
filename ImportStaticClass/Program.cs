﻿using System;
using static System.Console;
using static System.Math;

namespace ImportStaticClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Write("r = ");
            double.TryParse(ReadLine(), out var x);
            WriteLine($"S = {PI * Pow(x, 2)}");
            ReadKey();
        }
    }
}
